#!/bin/sh

DEBIAN=stretch
DOCTEST=master

set -e

printf '%s\n' \
	"deb http://ftp.debian.org/debian $DEBIAN-backports main" \
	> /etc/apt/sources.list.d/$DEBIAN-backports.list

apt-get update

apt-get dist-upgrade -y

# backported cmake requires backported libuv1
apt-get install --no-install-recommends -y \
	binutils \
	ca-certificates \
	cmake/$DEBIAN-backports \
	curl \
	g++ \
	gcc \
	git \
	lcov \
	libc-dev \
	libuv1/$DEBIAN-backports \
	make

mkdir -p /usr/local/include/doctest
curl -Lf \
	-o /usr/local/include/doctest/doctest.h \
	https://git.mel.vin/mirror/doctest/raw/$DOCTEST/doctest/doctest.h

apt-get purge -y \
	curl

apt-get autoremove -y
apt-get clean
rm -rf /var/lib/apt/lists/*
