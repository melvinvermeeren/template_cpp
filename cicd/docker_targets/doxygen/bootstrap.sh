#!/bin/sh

DEBIAN=stretch

set -e

printf '%s\n' \
	"deb http://ftp.debian.org/debian $DEBIAN-backports main" \
	> /etc/apt/sources.list.d/$DEBIAN-backports.list

apt-get update

apt-get dist-upgrade -y

# backported cmake requires backported libuv1
apt-get install --no-install-recommends -y \
	ca-certificates \
	cmake/$DEBIAN-backports \
	doxygen \
	git \
	graphviz \
	libuv1/$DEBIAN-backports \
	make

apt-get autoremove -y
apt-get clean
rm -rf /var/lib/apt/lists/*
