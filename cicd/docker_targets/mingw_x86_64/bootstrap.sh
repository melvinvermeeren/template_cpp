#!/bin/sh

DEBIAN=stretch

set -e

printf '%s\n' \
	"deb http://ftp.debian.org/debian $DEBIAN-backports main" \
	> /etc/apt/sources.list.d/$DEBIAN-backports.list

apt-get update

apt-get dist-upgrade -y

# backported cmake requires backported libuv1
apt-get install --no-install-recommends -y \
	binutils-mingw-w64-x86-64 \
	ca-certificates \
	cmake/$DEBIAN-backports \
	g++-mingw-w64-x86-64 \
	gcc-mingw-w64-x86-64 \
	git \
	libuv1/$DEBIAN-backports \
	make

apt-get autoremove -y
apt-get clean
rm -rf /var/lib/apt/lists/*
